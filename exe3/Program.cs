﻿//Autores Lucas Lazarim e Otto Carvalho R.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace exe3
{
    class Program
    {
        static void Main(string[] args)
        {
            float a, b;
            Console.WriteLine("Digite um valor para A:");
            a = float.Parse(Console.ReadLine());
            Console.WriteLine("Digite um valor para B:");
            b = float.Parse(Console.ReadLine());

            bool equal = Math.Abs(a - b) < 0.000001;
            if (equal) { 
            Console.WriteLine("Os números podem ser considerados como Iguais!");
            }
            else { 
             Console.WriteLine("Os números não podem ser considerados como Iguais!");
            }
            Console.ReadKey();
        }
       
    }
}
